/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2019 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "dma.h"
#include "spi.h"
#include "gpio.h"
#include "fsmc.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "lcd.h"
#include "ws2812.h"
#include "Adafruit_NeoPixel.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */
// Input a value 0 to 255 to get a color value.
// The colours are a transition r - g - b - back to r.
uint32_t Wheel(uint8_t WheelPos)
{
    WheelPos = 255 - WheelPos;
    if(WheelPos < 85)
    {
        return adafruit_neopixel.Color_rgb(255 - WheelPos * 3, 0, WheelPos * 3);
    }
    if(WheelPos < 170)
    {
        WheelPos -= 85;
        return adafruit_neopixel.Color_rgb(0, WheelPos * 3, 255 - WheelPos * 3);
    }
    WheelPos -= 170;
    return adafruit_neopixel.Color_rgb(WheelPos * 3, 255 - WheelPos * 3, 0);
}

// Fill the dots one after the other with a color
void colorWipe(uint32_t c, uint8_t wait)
{
    for(uint16_t i=0; i<adafruit_neopixel.numPixels(); i++)
    {
        adafruit_neopixel.setPixelColor(i, c);
        adafruit_neopixel.show();
        HAL_Delay(wait);
    }
}

//Theatre-style crawling lights.
void theaterChase(uint32_t c, uint8_t wait)
{
    for (int j=0; j<10; j++)//do 10 cycles of chasing
    {  
        for (int q=0; q < 3; q++)
        {
            for (uint16_t i=0; i < adafruit_neopixel.numPixels(); i=i+3)
            {
                adafruit_neopixel.setPixelColor(i+q, c);//turn every third pixel on
            }
            adafruit_neopixel.show();

            HAL_Delay(wait);

            for (uint16_t i=0; i < adafruit_neopixel.numPixels(); i=i+3)
            {
                adafruit_neopixel.setPixelColor(i+q, 0);//turn every third pixel off
            }
        }
    }
}

void rainbow(uint8_t wait)
{
    uint16_t i, j;

    for(j=0; j<256; j++)
    {
        for(i=0; i<adafruit_neopixel.numPixels(); i++)
        {
            adafruit_neopixel.setPixelColor(i, Wheel((i+j) & 255));
        }
        adafruit_neopixel.show();
        HAL_Delay(wait);
    }
}

// Slightly different, this makes the rainbow equally distributed throughout
void rainbowCycle(uint8_t wait)
{
    uint16_t i, j;

    for(j=0; j<256*3; j++)// 3 cycles of all colors on wheel
    { 
        for(i=0; i< adafruit_neopixel.numPixels(); i++)
        {
            adafruit_neopixel.setPixelColor(i, Wheel(((i * 256 / adafruit_neopixel.numPixels()) + j) & 255));
        }
        adafruit_neopixel.show();
        HAL_Delay(wait);
    }
}

//Theatre-style crawling lights with rainbow effect
void theaterChaseRainbow(uint8_t wait)
{
    for (int j=0; j < 256; j++)// cycle all 256 colors in the wheel
    {     
        for (int q=0; q < 3; q++)
        {
            for (uint16_t i=0; i < adafruit_neopixel.numPixels(); i=i+3)
            {
                adafruit_neopixel.setPixelColor(i+q, Wheel( (i+j) % 255));    //turn every third pixel on
            }
            adafruit_neopixel.show();

            HAL_Delay(wait);

            for (uint16_t i=0; i < adafruit_neopixel.numPixels(); i=i+3)
            {
                adafruit_neopixel.setPixelColor(i+q, 0);        //turn every third pixel off
            }
        }
    }
}

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_DMA_Init();
  MX_FSMC_Init();
  MX_SPI1_Init();
  /* USER CODE BEGIN 2 */
    LCD_Init();
    POINT_COLOR = RED;
    LCD_ShowString(30,50,200,16,16,"STM32F103ZET6");    
    LCD_ShowString(30,70,200,16,16,"Adafruit Neopixel test");    
    LCD_ShowString(30,90,200,16,16,"Luke");
    LCD_ShowString(30,110,200,16,16,"2022/04/12");
    POINT_COLOR = BLUE;//设置字体为蓝色
    adafruit_neopixel.NeoPixel_op = WS2812_send;// 底层数据发送函数
    adafruit_neopixel.init(WS2812_LINK_COUNT, NEO_GRB + NEO_KHZ800);
    adafruit_neopixel.begin();
    adafruit_neopixel.setBrightness(50);
    adafruit_neopixel.show();

  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
    while (1)
    {
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
        // Some example procedures showing how to display to the pixels:
        colorWipe(adafruit_neopixel.Color_rgb(255, 0, 0), 50); // Red
        colorWipe(adafruit_neopixel.Color_rgb(0, 255, 0), 50); // Green
        colorWipe(adafruit_neopixel.Color_rgb(0, 0, 255), 50); // Blue
        //colorWipe(strip.Color(0, 0, 0, 255), 50); // White RGBW
        // Send a theater pixel chase in...
        theaterChase(adafruit_neopixel.Color_rgb(127, 127, 127), 100); // White
        theaterChase(adafruit_neopixel.Color_rgb(127, 0, 0), 100); // Red
        theaterChase(adafruit_neopixel.Color_rgb(0, 0, 127), 100); // Blue

        rainbow(20);
        rainbowCycle(20);
        theaterChaseRainbow(60);
    }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.HSEPredivValue = RCC_HSE_PREDIV_DIV1;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL8;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV2;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
  {
    Error_Handler();
  }
}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */

  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
